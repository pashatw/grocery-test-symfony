<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductPrice;

class CatalogController extends Controller
{
	public function index(Request $req)
	{
		$q = $req->get('q');
		$products = [];
		if (@$q) {
			$repository = $this->getDoctrine()->getRepository(Product::class);
			$products = $repository->createQueryBuilder('a')
                   ->where('a.name LIKE :name')
                   ->setParameter('name', '%'.$q.'%')
                   ->setMaxResults(10)
                   ->getQuery()
                   ->getResult();

			// foreach ($products as $key => $value) {
			// 	echo $value->getImg_url();
			// }
			// print_r($products);
			// exit();
		}

		$data = array();
		$data['products'] = $products;
		$data['q'] = @$q;
		// print_r($data);exit();
		return $this->render('pages/groceries.html.twig', $data);
	}

	public function detail($slug,$id)
	{
		$product = $this->getDoctrine()
			->getRepository(Product::class)->findOneBy(array(
				'slug' => $slug,
				'id' => $id,
			));

		$price = $this->getDoctrine()->getRepository(ProductPrice::class)->createQueryBuilder('a')
                   ->where('a.id = :id')
                   ->setParameter('id', $product->getId())
                   ->orderBy('a.crawled_time', 'DESC')
                   ->getQuery()
                   ->getSingleResult();

        $data = array();
		$data['product'] = $product;
		$data['price'] = $price;
		// print_r($data);exit();
		return $this->render('pages/detail_grocery.html.twig', $data);

	}
}
