<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Product
 *
 * @ORM\Table(name="product")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductRepository")
 */
class Product
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /** 
      * @ORM\Column(type = "string") 
    */ 
    private $name;
    /** 
      * @ORM\Column(type = "text") 
    */ 
    private $img_url;
    /** 
      * @ORM\Column(type = "string") 
    */ 
    private $slug;
    /** 
      * @ORM\Column(type = "string") 
    */ 
    private $brand;
    /** 
      * @ORM\Column(type = "string") 
    */ 
    private $unit;
    /** 
      * @ORM\Column(type = "string") 
    */ 
    private $description;
    /** 
      * @ORM\Column(type = "text") 
    */ 
    private $feature;
    /** 
      * @ORM\Column(type = "text") 
    */ 
    private $usage_instruction;

    /**
     * @ORM\ManyToOne(targetEntity="ProductPrice", inversedBy="product")
     * @ORM\JoinColumn(name="id", referencedColumnName="id")
     */
    // private $price;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    public function getName()
    {
        return $this->name;
    }
    public function getImg_url()
    {
        return $this->img_url;
    }
    public function getSlug()
    {
        return $this->slug;
    }
    public function getBrand()
    {
        return $this->brand;
    }
    public function getUnit()
    {
        return $this->unit;
    }
    public function getDescription()
    {
        return $this->description;
    }
    public function getFeature()
    {
        return $this->feature;
    }
    public function getUsage_instruction()
    {
        return $this->usage_instruction;
    }
}

