<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductPrice
 *
 * @ORM\Table(name="product_price")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductPriceRepository")
 */
class ProductPrice
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /** 
      * @ORM\Column(type = "datetime") 
    */ 
    private $crawled_time;
    /** 
      * @ORM\Column(type = "float") 
    */ 
    private $price;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    public function getCrawled_time()
    {
        return $this->crawled_time;
    }
    public function getPrice()
    {
        return $this->price;
    }
}

